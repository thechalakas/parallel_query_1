﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parallel_Query_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //first let me get an array on which I can do some query operations

            var list_of_numbers = Enumerable.Range(0, 100);
            var result_list_numbers = list_of_numbers;

            //now running that parallel query
            //just to add x indicates each element in the list
            //the x will be sent to temp as an argument
            //the temp will return true or false or raise an exception 
            //if it is true, that x item will be added to list
            //if it is false, that x item will NOT be added to list
            try
            {
                result_list_numbers = list_of_numbers.AsParallel().Where(x => temp(x));
            }
            catch(AggregateException e)
            {

                    Console.WriteLine("{0}. error messages are ", e.Message.Count() );

            }

            //now let me display the result list
            for(int i=0;i<result_list_numbers.Count(); i++)
            {
                Console.WriteLine("{0}.  {1}", i, result_list_numbers.ElementAt(i));
            }

            //keep the console from dissapearing
            Console.ReadLine();

        }

        private static bool temp(int x)
        {
            if(x%2 == 0)
            {
                return true;
            }
            else if(x%3 ==0)
            {
                //string exception_message = " what went bad is  " + x;
                //throw new ArgumentException(exception_message);
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
